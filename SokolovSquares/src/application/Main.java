package application;

import java.util.Random;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
//https://stackoverflow.com/questions/40729967/drawing-shapes-on-javafx-canvas
public class Main extends Application {
	
	// rounded edges
	private static final int SQUARE_ROUNDING = 10;
	
	public static double primaryStageWidth;
	public static double primaryStageHeight;
	
	GraphicsContext graphicsContext;

	

	@Override
	public void start(Stage primaryStage) {

		Canvas canvas = createCanvas();		

		// Get the graphics context of the canvas
		graphicsContext = canvas.getGraphicsContext2D();


		// Draw a Text
		//graphicsContext.strokeText("Hello Canvas", 150, 100);

		Pane rootPane = createPane();


		// Add the Canvas to the Pane
		rootPane.getChildren().add(canvas);
		// Create the Scene
		
		Scene scene = new Scene(rootPane);
		// Add the Scene to the Stage
		setUpPrimaryStage(primaryStage, scene);
		canvas.heightProperty () .bind (primaryStage.heightProperty ());
		canvas.widthProperty () .bind (primaryStage.widthProperty ());
		primaryStageWidth = primaryStage.getWidth();
		primaryStageHeight = primaryStage.getHeight();

		//pauseExecution(2000);
		
		
		doDrawings();
		
	}

	private void doDrawings() {
		
		Square[] square = {Square.TOP_LEFT, Square.TOP_MIDDLE, Square.TOP_RIGHT, 
				Square.MID_LEFT, Square.MID_MIDDLE, Square.MID_RIGHT, 
				Square.BOTTOM_LEFT, Square.BOTTOM_MIDDLE, Square.BOTTOM_RIGHT};

		int rndInt = new Random().nextInt(8) + 1;
		
		square[rndInt].drawSquare(Color.BLUE, graphicsContext);
		
		//testAllEnums_Squares();
		

	}

	private void testAllEnums_Squares() {
		Square.TOP_MIDDLE.drawSquare(Color.RED, graphicsContext);
		Square.TOP_LEFT.drawSquare(Color.RED, graphicsContext);
		Square.TOP_RIGHT.drawSquare(Color.RED, graphicsContext);
		Square.MID_LEFT.drawSquare(Color.RED, graphicsContext);
		Square.MID_MIDDLE.drawSquare(Color.RED, graphicsContext); 
		Square.MID_RIGHT.drawSquare(Color.RED, graphicsContext);
		Square.BOTTOM_LEFT.drawSquare(Color.RED, graphicsContext);
		Square.BOTTOM_MIDDLE.drawSquare(Color.RED, graphicsContext);
		Square.BOTTOM_RIGHT.drawSquare(Color.RED, graphicsContext);
	}

	private void pauseExecution(int ms) {
		try {
			 Thread.sleep(ms);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void setUpPrimaryStage(Stage primaryStage, Scene scene) {
		primaryStage.setScene(scene);
		// Set the Title of the Stage
		primaryStage.setTitle("Creation of a Canvas");
		primaryStage.setFullScreen(true);
		// Display the Stage
		primaryStage.show();
	}

	private Pane createPane() {
		// Create the Pane
		Pane root = new Pane();
		// Set the Style-properties of the Pane
		// root.setStyle("-fx-padding: 10;" +
		// "-fx-border-style: solid inside;" +
		// "-fx-border-width: 2;" +
		// "-fx-border-insets: 5;" +
		// "-fx-border-radius: 5;" +
		// "-fx-border-color: blue;");
		return root;
	}

	private Canvas createCanvas() {
		// Create the Canvas
		Canvas canvas = new Canvas(400, 200);
		// Set the width of the Canvas
		canvas.setWidth(400);
		// Set the height of the Canvas
		canvas.setHeight(200);
		return canvas;
	}

	private void drawSquare(int xPos, int yPos, int sideSize, Color color) {
		// Change the fill color
		graphicsContext.setFill(color);
		// Draw a filled rounded Rectangle
		graphicsContext.fillRoundRect(xPos, yPos, sideSize, sideSize, SQUARE_ROUNDING, SQUARE_ROUNDING);
	}

	public static void main(String[] args) {
		launch(args);
	}
}
