package application;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public enum Square {
	
	TOP_LEFT {
		@Override
		void drawSquare(Color color, GraphicsContext graphicsContext) {
			long xPos = horizontalOffsetFromLeftEndOfScreen;
			long yPos = topRowVerticalOffset;
			drawSquare(xPos, yPos, SQUARE_SIZE, color, graphicsContext);
		}
	}, 
	
	TOP_MIDDLE {
		@Override
		void drawSquare(Color color, GraphicsContext graphicsContext) {
			long xPos = Math.round(Main.primaryStageWidth/2 - SQUARE_SIZE/2);
			long yPos = topRowVerticalOffset;
			drawSquare(xPos, yPos, SQUARE_SIZE, color, graphicsContext);
		}
	}, 
	
	TOP_RIGHT {
		@Override
		void drawSquare(Color color, GraphicsContext graphicsContext) {			
			long xPos = Math.round(Main.primaryStageWidth - SQUARE_SIZE - horizontalOffsetFromRightEndOfScreen);
			long yPos = topRowVerticalOffset;
			drawSquare(xPos, yPos, SQUARE_SIZE, color, graphicsContext);
		}
	},
		
	MID_LEFT {
		@Override
		void drawSquare(Color color, GraphicsContext graphicsContext) {
			long xPos = horizontalOffsetFromLeftEndOfScreen;
			long yPos = Math.round(Main.primaryStageHeight/2 - SQUARE_SIZE/2);
			drawSquare(xPos, yPos, SQUARE_SIZE, color, graphicsContext);
		}
	},
	
	MID_MIDDLE {
		@Override
		void drawSquare(Color color, GraphicsContext graphicsContext) {
			long xPos = Math.round(Main.primaryStageWidth/2 - SQUARE_SIZE/2);
			long yPos = Math.round(Main.primaryStageHeight/2 - SQUARE_SIZE/2);
			drawSquare(xPos, yPos, SQUARE_SIZE, color, graphicsContext);
		}
	},
	
	MID_RIGHT {
		@Override
		void drawSquare(Color color, GraphicsContext graphicsContext) {
			long xPos = Math.round(Main.primaryStageWidth - SQUARE_SIZE - horizontalOffsetFromRightEndOfScreen);
			long yPos = Math.round(Main.primaryStageHeight/2 - SQUARE_SIZE/2);
			drawSquare(xPos, yPos, SQUARE_SIZE, color, graphicsContext);
		}
	},
	
	BOTTOM_LEFT {
		@Override
		void drawSquare(Color color, GraphicsContext graphicsContext) {			
				long xPos = horizontalOffsetFromLeftEndOfScreen;
				long yPos = Math.round(Main.primaryStageHeight - SQUARE_SIZE - bottomRowVerticalOffset);
				drawSquare(xPos, yPos, SQUARE_SIZE, color, graphicsContext);
		}
	},
	
	BOTTOM_MIDDLE {
		@Override
		void drawSquare(Color color, GraphicsContext graphicsContext) {
			long xPos = Math.round(Main.primaryStageWidth/2 - SQUARE_SIZE/2);
			long yPos = Math.round(Main.primaryStageHeight - SQUARE_SIZE - bottomRowVerticalOffset);
			drawSquare(xPos, yPos, SQUARE_SIZE, color, graphicsContext);
		}
	},
	
	BOTTOM_RIGHT {
		@Override
		void drawSquare(Color color, GraphicsContext graphicsContext) {
			long xPos = Math.round(Main.primaryStageWidth - SQUARE_SIZE - horizontalOffsetFromRightEndOfScreen);
			long yPos = Math.round(Main.primaryStageHeight - SQUARE_SIZE - bottomRowVerticalOffset);
			drawSquare(xPos, yPos, SQUARE_SIZE, color, graphicsContext);
		}
	};
	
	// rounded edges
	private static final int SQUARE_ROUNDING = 10;
	
	private static final int SQUARE_SIZE = 155;
	private static final int horizontalOffsetFromRightEndOfScreen = 85;
	private static final int horizontalOffsetFromLeftEndOfScreen = 55;
	private static final int topRowVerticalOffset = 55;
	private static final int bottomRowVerticalOffset = 65;
	
	abstract void drawSquare(Color color, GraphicsContext graphicsContext);
	
	protected void drawSquare(long xPos, long yPos, int sideSize, Color color, GraphicsContext graphicsContext) {
		// Change the fill color
		graphicsContext.setFill(color);
		// Draw a filled rounded Rectangle
		graphicsContext.fillRoundRect(xPos, yPos, sideSize, sideSize, SQUARE_ROUNDING, SQUARE_ROUNDING);
	}

}
